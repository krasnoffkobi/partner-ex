import { Interface } from "readline";
import { ClientRequest } from "http";
export interface IClients {
    firstName: string;
    idNumber: number;
    subscriptionsList: string;
}