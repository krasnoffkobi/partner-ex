import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(private http: HttpClient) { }

  public getClientsData() {
    return this.http.get('/assets/data/clients.json').toPromise();
  }

  public getSubscriptionsData(clientId: string) {
    return this.http.get('/assets/data/subscriptions.json').toPromise();
  }

  public getPackagesData(SubscriptionId: string) {
    return this.http.get('/assets/data/packages.json').toPromise();
  }
}
