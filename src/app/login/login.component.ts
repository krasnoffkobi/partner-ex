import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalDataService } from '../global-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userName: string;

  constructor(private router: Router, protected gd: GlobalDataService) { }

  ngOnInit(): void {
  }

  submitForm() {
    if (this.userName !== "") {
      this.gd["isLogin"] = true;

      let someDate = new Date();
      const numberOfDaysToAdd = 2;
      someDate.setDate(someDate.getDate() + numberOfDaysToAdd); 

      const object = {isLogin: this.gd["isLogin"], timestamp: someDate.getTime()}
      localStorage.setItem("isLogin", JSON.stringify(object));
      this.router.navigate(["/clientPage"]);
    }
  }

}
