import { Injectable } from '@angular/core';

interface ShareObj {
  [id: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export class GlobalDataService {
  shareObj: ShareObj = {};

  constructor() { }
}
