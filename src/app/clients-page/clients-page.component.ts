import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { MatSelectChange } from '@angular/material/select';

export interface PackagesList {
  packageName: string;
  packageId: string;
  quantity: number;
  consumption: number;
}

@Component({
  selector: 'app-clients-page',
  templateUrl: './clients-page.component.html',
  styleUrls: ['./clients-page.component.scss']
})
export class ClientsPageComponent implements OnInit {

  clientsList: any;
  subscriptionsList: any;
  packagesList: any = [];
  displayedColumns = ['packageName', 'packageId', 'quantity', 'consumption'];

  constructor(public getDataService: GetDataService) { }

  ngOnInit(): void {
    this.init();
  }

  async init() {
    this.clientsList = await this.getDataService.getClientsData();
    console.log('this.clientsList', this.clientsList);
  }

  async selectClient(event: MatSelectChange) {
    console.log(event);
    this.subscriptionsList = await this.getDataService.getSubscriptionsData(event.value.idNumber);
  }

  async selectPackages(event: MatSelectChange) {
    console.log(event);
    this.packagesList = await this.getDataService.getPackagesData(event.value.subscriptionId);
    console.log(this.packagesList);
  }



}
