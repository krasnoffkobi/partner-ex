import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GlobalDataService } from './global-data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate {
  constructor(private router: Router, protected gd: GlobalDataService) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const isLogin = JSON.parse(localStorage.getItem("isLogin"));
      this.gd["isLogin"] = false;
      const today = new Date()
      if (isLogin.isLogin === true && today.getTime() < isLogin.timestamp)
        this.gd["isLogin"] = true;
      
      if (this.gd["isLogin"] == true)
      {
        return true;
      }
      else {
        this.router.navigate(["/login", state.url.replace(/\//gi, "")]);
        return false;
      }
    }
  
}
